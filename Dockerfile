FROM ruby:2.6.4

RUN apt-get update -q
RUN apt-get install -qy nginx
RUN apt-get install -qy curl
RUN apt-get install -qy nodejs
RUN apt-get install -y --force-yes libpq-dev
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

RUN chown -R www-data:www-data /var/lib/nginx

RUN gem install bundler
RUN gem install foreman
RUN gem install rails

ADD config/container/nginx-sites.conf /etc/nginx/sites-enabled/default

ADD ./ /rails

WORKDIR /rails

ENV RAILS_ENV production
ENV PORT 8000

RUN /bin/bash -l -c "bundle install --without development test"

EXPOSE 8000

CMD bundle exec rake db:setup db:migrate && foreman start -f Procfile